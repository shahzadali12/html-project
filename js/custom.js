$(document).ready(function(){
	"use strict";
    /*================================================
      pet ccompany slider start
  =================================================*/ 

  if($('.banner-slider').length){
    $('.banner-slider').slick({
    slidesToShow: 1,
    autoplay: true,
    fade:true,
    arrow:false,
    dots:true,
    responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll:1,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }   
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
  }
    


  /*================================================
      slider start
  =================================================*/ 
  if($('.testimonial-slider').length){
    $('.testimonial-slider').slick({
    slidesToShow: 1,
    autoplay: false,
    arrow:false,
    dots:true,
    autoplaySpeed: 2000,
    responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll:1,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
  }
 
    /*================================================
      slider start
  =================================================*/ 
  if($('.latest_articles_slider').length){
    $('.latest_articles_slider').slick({
    slidesToShow: 3,
    autoplay: false,
    arrow:false,
    autoplaySpeed: 2000,
    responsive: [
        {
          breakpoint: 1324,
          settings: {
            slidesToShow: 2,
            slidesToScroll:2,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
  
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
    
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
  }

    /*
    ==============================================================
        DL Responsive Menu
    ==============================================================
    */
    if(typeof($.fn.dlmenu) == 'function'){
      $('#kode-responsive-navigation').each(function(){
        $(this).find('.dl-submenu').each(function(){
        if( $(this).siblings('a').attr('href') && $(this).siblings('a').attr('href') != '#' ){
          var parent_nav = $('<li class="menu-item kode-parent-menu"></li>');
          parent_nav.append($(this).siblings('a').clone());

          $(this).prepend(parent_nav);
        }
        });
        $(this).dlmenu();
      });
    }
  

   
	/*
	==============================================================
	 COUNTDOWN  Script Start
	==============================================================
	*/
	
	if($('.countdown').length){
		$('.countdown').downCount({ date:'8/8/2018 12:00:00', offset: +1 });
	}
 
	/*
  ==============================================================
     Counter Script Start
  ==============================================================
  */
    if($('.counter').length){
        $('.counter').counterUp({
          delay: 20,
          time: 1000
        });
    }
	
	

  /*
    =======================================================================
         Pretty Photo Script Script
    =======================================================================
  */
    if($("a[data-rel^='prettyPhoto']").length){
      $("a[data-rel^='prettyPhoto']").prettyPhoto();
    }
 

});
  
	
	jQuery(document).ready(function() {
		jQuery('.tabs .tab-links a').on('click', function(e) {
			var currentAttrValue = jQuery(this).attr('href');

			// Show/Hide Tabs
			jQuery('.tabs ' + currentAttrValue).show().siblings().hide();

			// Change/remove current tab to active
			jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

			e.preventDefault();
			
			// Show/Hide Tabs
			jQuery('.tabs ' + currentAttrValue).siblings().slideUp(800);
			jQuery('.tabs ' + currentAttrValue).delay(800).slideDown(800);
			
			// Show/Hide Tabs
			jQuery('.tabs ' + currentAttrValue).fadeIn(400).siblings().hide();
		});
	});
  
$('#demo').dcalendarpicker();


   


  


	